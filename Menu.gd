extends CanvasLayer
signal play_game

func _ready():
	pass

func _on_Button_pressed():
	$Info.hide()
	$Button.hide()
	emit_signal("play_game")

func update_score(score):
	$Score.text = "Distance: "+str(score)+"m"
	
func reset():
	$Info.text = "Game Over!"
	$MessageTimer.start()
	$Info.show()
	$Button.show()

func update_best(score):
	$Best.text = "Best: "+str(score)+"m"

func _on_MessageTimer_timeout():
	$Info.text = "Dodge the trees!\nControls:\nUp Arrow - Move up"
