extends Area2D

signal collide

export (int) var VELOCITY # How fast the plane goes up
export (int) var GRAVITY # How fast the plane falls
export (int) var TILT # The degree of rotation of the plane from the horizontal axis while going up/down
var dead = false

var screensize = get_viewport_rect().size
var angle = 0

func _ready():
	hide()
	screensize = get_viewport_rect().size

func _process(delta):
	if Input.is_action_pressed("ui_up") and !dead:
		position.y -= VELOCITY*delta
		if angle != -TILT:
			rotate(deg2rad(-TILT))
			angle -= TILT
	else:
		position.y += GRAVITY*delta
		if angle != TILT:
			rotate(deg2rad(TILT))
			angle += TILT
			
	position.y = clamp(position.y, 0, screensize.y)
	

func play_game():
	show()
	$AnimatedSprite.animation = "fly"
	$CollisionPolygon2D.disabled = false
	dead = false
	$AnimatedSprite.play()

func _on_Plane_body_entered(body):
	$CollisionPolygon2D.disabled = true
	$AnimatedSprite.animation = "crash"
	dead = true
	emit_signal("collide")
