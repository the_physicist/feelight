extends Node2D

export (PackedScene) var Obstacle
export (int) var START_VELOCITY
var OBSTACLE_VELOCITY
var score
var best = 0

func _ready():
	randomize()
	var bgNum = randi()%3 + 1;
	$Background.texture = load("res://sprites/background"+str(bgNum)+".png");
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass

func start():
	OBSTACLE_VELOCITY = START_VELOCITY
	$MenuMusic.stop()
	score = 0
	$HUD.update_score(score)
	$Player.play_game()
	$Player.position = $StartPosition.position
	$SpawnTimer.start()
	$ScoreTimer.start()
	$GameOver.stop()
	if randi()%2 == 0:
		$Gameplay1.play()
	else:
		$Gameplay2.play()
	
	for child in $EnemyContainer.get_children():
		child.queue_free()


func _on_SpawnTimer_timeout():
	var obst = Obstacle.instance()
	var scaleFactor = rand_range(1,2.1);
	
	$EnemyContainer.add_child(obst)
	
	obst.scale_bottom(scaleFactor)
	
	if randi()%2 == 1:
		obst.rotate(PI)
		obst.position = $TopPosition.position
	else:
		obst.position = $BottomPosition.position

	obst.set_velocity(OBSTACLE_VELOCITY)
	

func _on_ScoreTimer_timeout():
	score += 25
	$HUD.update_score(score)
	if score%250 == 0 and OBSTACLE_VELOCITY < 750:
		OBSTACLE_VELOCITY += 50


func game_over():
	$Crash.play()
	$SpawnTimer.stop()
	$ScoreTimer.stop()
	$Gameplay1.stop()
	$Gameplay2.stop()
	$GameOver.play()
	
	if score > best:
		$HUD.update_best(score)
		best = score
	
	for child in $EnemyContainer.get_children():
		child.set_velocity(0)
	$HUD.reset()
