This is a 2D game similar to Flappy Bird: you ride a plane and try to avoid the trees; as simple as that.

Further pushes to the repository will be bug fixes, gameplay improvements and possible feature updates.

The game is written using Godot Engine 3.0.4 as of this now using the GDScript language.

The media used in the game isn't my work. Please check the "MEDIA CREDITS" file for more information. The code and the level design however is all of my work.