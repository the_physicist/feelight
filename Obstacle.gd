extends KinematicBody2D
var velocity


func _ready():
	pass
	
func _physics_process(delta):
	move_and_slide(Vector2(-velocity,0))

func set_velocity(vel):
	velocity = vel

func scale_bottom(factor):
	$Lower.set_scale(Vector2($Lower.get_scale().x, factor))

func _on_Visibility_screen_exited():
	$DeleteTimer.start()


func _on_DeleteTimer_timeout():
	queue_free()
